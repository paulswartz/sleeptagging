'use strict';
Router.configure({
  autoRender: false,
  loadingTemplate: 'loading'
});

Router.map(function () {

  function requireLogin() {
    /*jshint validthis: true */
    if (!Meteor.userId()) {
      this.redirect('jumbotron');
    } else {
      this.render();
    }
  }

  function days() {
    return Meteor.subscribe('daysWithMainSleep');
  }

  this.route('jumbotron', {
    path: '/',
    template: 'jumbotron',
    action: function() {
      if (Meteor.userId()) {
       this.redirect('add');
      } else {
        this.render();
      }
    }
  });

  this.route('add', {
    path: '/add',
    waitOn: function() { return Meteor.subscribe(
      'day', window.today().format('YYYY-MM-DD')); },
    action: function() {
      requireLogin.call(this);
      Session.set('clickedTags', undefined);
    }
  });

  this.route('history', {
    path: '/history',
    waitOn: days,
    action: requireLogin
  });

  this.route('advice_bar', {
    path: '/advice/by_tag',
    waitOn: days,
    action: requireLogin,
    data: {
      title: "Change in average % asleep by Tag"
    }
  });
  
  this.route('advice', {
    path: '/advice/by_bed_time',
    waitOn: days,
    action: requireLogin,
    data: {
      title: "% asleep by bed time",
      tooltipAttribute: "date",
      x: function(d) {
        var m = moment(window.mainSleep(d).startTime);
        var hours = m.hours();
        if (hours < 12) {
          hours = hours + 24;
        }
        return 3600 * hours + 60 * m.minutes() + m.seconds();
      },
      xDisplay: function(v) {
        var m = moment.utc(v * 1000);
        return m.format('h:mm a');
      }
    }
  });

  this.route('advice', {
    path: '/advice/by_time_asleep',
    waitOn: days,
    action: requireLogin,
    data: {
      title: "% asleep by time asleep",
      tooltipAttribute: "date",
      x: function(d) {
        return d.sleep.summary.totalMinutesAsleep;
      },
      xDisplay: function(v) {
        return window.formatMinutes(v);
      }
    }
  });

  this.route('advice', {
    path: '/advice/by_steps',
    waitOn: days,
    action: requireLogin,
    data: {
      title: "% asleep by previous day's steps",
      tooltipAttribute: "date",
      x: function(d) {
        return d.activities.summary.steps;
      },
      xDisplay: function(v) {
        return v;
      }
    }
  });

  this.route('advice', {
    path: '/advice/by_date',
    waitOn: days,
    action: requireLogin,
    data: {
      title: "% asleep by date",
      tooltipAttribute: "steps",
      x: function(d) {
        return +moment(d.date);
      },
      xDisplay: function(v) {
        return moment(v).format('MMM DD');
      }
    }
  });
});

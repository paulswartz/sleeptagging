'use strict';
_ = lodash;

window.today = function () {
  return moment().add("hours", 3);
};

window.mainSleep = function (day) {
  if (!day.sleep) {
    return null;
  }
  return _.find(day.sleep.sleep, 'isMainSleep');
};

window.formatMinutes = function(minutes) {
  var m = moment.utc(minutes * 60 * 1000),
      formattedMinutes = m.format('mm');
    return (m.date() - 1) * 24 + m.hours() + ':' + formattedMinutes;

};

window.showAlert = function(type, message) {
  function newDisplay() {
    var display = $("<div></div>").prependTo("body > div.container");
    display.attr('class', 'alert alert-' + type)
    .text(message);
    Meteor.setTimeout(function() {
      display.hide(function() { this.remove(); });
    }, 5000);
  }

  var display = $("body > div.container > div.alert-" + type);
  if (display.length) {
    display.hide(function() { this.remove(); newDisplay(); });
  } else {
    newDisplay();
  }
};

window.showAlertCallback = function(message) {
  return function(error) {
    if (error) {
      window.showAlert('danger', error);
    } else {
      window.showAlert('success', message);
    }
  };
};

Meteor.startup(function() {
  UserSession.set('needsSync', true);
});

Deps.autorun(function() {
  if (UserSession.get('needsSync')) {
    Meteor.call('syncSleep');
  }
});

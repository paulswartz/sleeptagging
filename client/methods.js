'use strict';
Meteor.methods({
  setTags: function(date, tags) {
    Days.update({date: date},
                {$set: {tags: tags}});
    Meteor.call('showAlert', 'success', 'Saved!');
  }
});

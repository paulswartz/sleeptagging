'use strict';
_ = lodash;

Template.tags.events({
  "click .add": function (ev) {
    var date = this.date;
    ev.preventDefault();
    Session.set('editing_' + date, true);
    Meteor.setTimeout(function() {
      $("#tags_" + date).focus();
    }, 100);
  },

  "keydown .form-control, click .save": function(ev) {
    if (ev.keyCode && ev.keyCode !== 13) {
      return;
    }
    ev.preventDefault();
    var newTags = $("#tags_" + this.date).val().split(/\s+/);
    Session.set('editing_' + this.date, false);
    Meteor.call('setTags', this.date, newTags,
                window.showAlertCallback('Saved!'));
  }
});

Template.tags.helpers({
  editing: function() {
    return Session.get('editing_' + this.date);
  },

  tags: function() {
    return this.tags || [];
  },

  tagString: function() {
    return (this.tags || []).join(' ');
  }
});

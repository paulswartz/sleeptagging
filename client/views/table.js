'use strict';
Template.table.helpers({
  days: function () {
    var latest = moment().add("hours", 3).format('YYYY-MM-DD');
    return Days.find({date: {$lte: latest}},
                     {sort: {date: -1}});
  }
});

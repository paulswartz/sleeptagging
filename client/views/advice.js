'use strict';
Template.advice.title = function() {
  return Router.getData().title;
};

Template.advice.active = function(name) {
  var current = Router.current();
  var regex = RegExp(name + '$');
  if (regex.exec(current.path)) {
    return 'active';
  }
  return '';
};

Template.advice.rendered = function () {
  var routerData = Router.getData();
  var padding = 20,
      width = $("#advice_wrapper").width() - padding,
      aspect = 8 / 3,
      height = Math.round(width / aspect);
  var svg = d3.select("#advice")
        .attr("width", width)
        .attr("height", height);

  if (!routerData.x) {
    return;
  }
  var x = routerData.x;
  function y(d) {
    return (d.sleep.summary.totalMinutesAsleep /
            d.sleep.summary.totalTimeInBed);
  }
  var computation = Deps.autorun(function() {
    var radius = d3.max([width / 500, 1]);
    var data = _.map(
      Days.find().fetch(), function(day) {
          return {
            x: x(day),
            y: y(day),
            date: moment(day.date).format('LL'),
            steps: day.activities.summary.steps,
            shape: 'circle',
            size: radius
          };
        });
    nv.addGraph(function() {
      var chart = nv.models.scatterChart()
            .showLegend(false)
            .showDistX(true)
            .showDistY(true)
            .useVoronoi(true);
      chart.xAxis.tickFormat(routerData.xDisplay);
      chart.yAxis.tickFormat(function(v) {
        return (v * 100).toFixed(0) + '%';
      });
      chart.tooltipContent(function(key, x, y, point) {
        var value = point.point[routerData.tooltipAttribute];
        if (routerData.tooltipAttribute === 'steps') {
          value = value + ' steps';
        }
        return value;
      });
      svg.datum([{key: '', values: data}])
        .call(chart);
      nv.utils.windowResize(chart.update);
      return chart;
    });
  });
};

'use strict';
function tags() {
  return Session.get('clickedTags') || {};
}

function setTags() {
  var newTags = _.reduce(tags(),
                         function(tags, clicked, tag) {
                           if (clicked) {
                             tags.push(tag);
                           }
                           return tags;
                         },
                         []);
  Meteor.call('setTags', window.today().format('YYYY-MM-DD'), newTags,
              window.showAlertCallback('Saved!'));
}

Template.add.events({
  'click .btn-tag': function() {
    var clickedTags = tags();
    clickedTags[this.tag] = !this.selected;
    Session.set('clickedTags', clickedTags);
    setTags();
  },
  'keydown #tags, click .btn-add': function(ev) {
    if (ev.keyCode && ev.keyCode !== 13) {
      return;
    }
    ev.preventDefault();
    var clickedTags = tags(),
        newTags = $("#tags").val().split(/\s+/),
        tagsMap = Session.get('tags');
    _.each(newTags, function(tag) {
      if (tag) {
        clickedTags[tag] = true;
        if (!tagsMap[tag]) {
          tagsMap[tag] = 1;
        }
      }
    });
    Session.set('clickedTags', clickedTags);
    setTags();
    $("#tags").val("");
    Session.set('tags', tags);
  }
});

Template.add.date = function() {
  return window.today().format('MMMM D, YYYY');
};

Template.add.day = function () {
  return Days.findOne({date: window.today().format('YYYY-MM-DD')});
};

Template.add.asleepTime = function() {
  return window.formatMinutes(this.sleep.summary.totalMinutesAsleep);
};

Template.add.tags = function() {
  var tagsMap = Session.get('tags');
  if (_.isUndefined(tagsMap)) {
    Meteor.call('getTags', function(e, r) {
      tagsMap = r;
      Session.set('tags', r);
    });
  }
  var clickedTags = Session.get('clickedTags');
  if (_.isUndefined(clickedTags)) {
    clickedTags = {};
    // look up the selected tags from the current day
    var day = Template.add.day();
    if (!_.isUndefined(day)) {
      _.map(day.tags, function(tag) {
        clickedTags[tag] = true;
      });
    }
    Session.set('clickedTags', clickedTags);
  }
  return _.sortBy(_.map(tagsMap, function(count, tag) {
    return {
      tag: tag,
      count: -count,
      selected: clickedTags[tag]
    };
  }), 'count');
};

'use strict';
_ = lodash;

var tooltipTemplate = _.template(
  '<h3><%- x %></h3>' +
    '<p><%- y %></p>' +
    '<p><%- count %> nights</p>'
);

Template.advice_bar.rendered = function () {
  var routerData = Router.getData();
  var svg = d3.select("#advice");

  function y(d) {
    return (d.sleep.summary.totalMinutesAsleep /
            d.sleep.summary.totalTimeInBed);
  }
  Template.advice.computation = Deps.autorun(function() {
    var days = Days.find().fetch();
    var tags = _.reduce(
      days,
      function(tags, day) {
        _.each(day.tags, function(tag) {
          if (!tag) {
            return;
          }
          if (!tags[tag]) {
            tags[tag] = {
              average: y(day),
              sum: y(day),
              count: 1
            };
          } else {
            tags[tag].sum = tags[tag].sum + y(day);
            tags[tag].count = tags[tag].count + 1;
            tags[tag].average = tags[tag].sum / tags[tag].count;
          }
        });
        return tags;
      },
      {});
    var overallAverage = _.reduce(days, function(acc, day) {
      if (!day.sleep.summary.totalTimeInBed ||
         !day.sleep.summary.totalMinutesAsleep) {
        return acc;
      }
      var percentAsleep = day.sleep.summary.totalMinutesAsleep /
            day.sleep.summary.totalTimeInBed;
      return {
        sum: acc.sum + percentAsleep,
        count: acc.count + 1,
        average: (acc.sum + percentAsleep) / (acc.count + 1)
      };
    }, {average: 0, sum: 0, count: 0}).average;
    var data = _.chain(tags).map(function(stats, tag) {
      return _.extend({
        label: tag,
        value: (stats.average - overallAverage) * Math.log(stats.count)
      }, stats);
    }).sortBy('value').reverse().value();
    nv.addGraph(function() {
      var chart = nv.models.discreteBarChart()
            .x(function(d) { return d.label; })
            .y(function(d) { return d.value; })
            .tooltips(true)
            .showValues(false);
      chart.yAxis.tickFormat(function(value) {
        var prefix = '+';
        if (value < 0) {
          prefix = '-';
          value = -value;
        }
        return prefix + (100 * value).toFixed(0) + '%';
      });
      chart.tooltipContent(function(key, x, y, point) {
        return tooltipTemplate({
          x: x,
          y: y,
          count: point.point.count
        });
      });
      d3.select('#advice')
        .datum([{key: '', values: data}])
        .call(chart);
      nv.utils.windowResize(chart.update);
      return chart;
    });
  });
};

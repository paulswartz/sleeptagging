'use strict';
function syncClick(ev) {
  if (ev) {
    ev.preventDefault();
  }
  if (!UserSession.get('syncingSleep')) {
    Meteor.call('syncSleep');
  }
}

Template.nav.helpers({
  active: function(template) {
    var current = Router.current();
    if (!current) {
      return "";
    }
    return current.template === template ? "active" : "";
  },
  syncing: function() {
    return UserSession.get('syncingSleep');
  }
});

Template.nav.events({
  "click #sync": syncClick
});

Meteor.autorun(function() {
  if (!Session.get('synced_this_session') && Meteor.user()) {
    Session.set('synced_this_session', true);
    // start syncing when the user has logged in
    syncClick();
  }
});

'use strict';
Template.day.helpers({
  steps: function() {
    if (!this.activities || !this.activities.summary) {
      return "";
    }
    return this.activities.summary.steps;
  },
  startTime: function() {
    var ms = window.mainSleep(this);
    if (ms) {
      return moment(ms.startTime).format('h:mm a');
    } else {
      return "";
    }
  },

  asleepTime: function() {
    if (!this.sleep || !this.sleep.summary ||
        !this.sleep.summary.totalTimeInBed) {
      return "";
    }
    return window.formatMinutes(this.sleep.summary.totalMinutesAsleep);
  },

  asleepPercentage: function () {
    if (!this.sleep || !this.sleep.summary ||
        !this.sleep.summary.totalTimeInBed) {
      return '';
    }
    var percent = (this.sleep.summary.totalMinutesAsleep /
                   this.sleep.summary.totalTimeInBed);
    return (percent * 100).toFixed(0) + '%';
  }
});

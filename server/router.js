'use strict';
var fs = Npm.require('fs');

Router.map(function () {
  this.route('fitbit_subscription', {
    path: '/_fitbit/subscription',
    where: 'server',

    action: function () {
      this.response.writeHead(204);
      this.response.end();
      var file = this.request.files.updates;
      var body = fs.readFileSync(file.path, 'utf8');
      console.log(body);
      _.each(JSON.parse(body), function(sub) {
        if (!Meteor.users.findOne({_id: sub.subscriptionId},
                                  {reactive: false})) {
          // skip if the user isn't registered
          return;
        }
        if (sub.collectionType !== 'activities' &&
            sub.collectionType !== 'sleep') {
          return;
        }
        var modifier = {date: sub.date,
                        user: sub.subscriptionId,
                        type: sub.collectionType};
        console.log('subscription',
                    sub.subscriptionId, sub.date, sub.collectionType);
        Syncs.upsert(modifier,
                     {$set: modifier});
        // calling UserSession.set doesn't work, so we do it manually
        UserSessionCollection.upsert({key: "needsSync",
                                      userId: sub.subscriptionId},
                                     {$set: {value: true}});
      });
    }
  });
});

'use strict';
// Meteor.publish("days", function() {
//   return Days.find({user: this.userId});
// });

Meteor.publish("daysWithMainSleep", function() {
  return Days.find({user: this.userId,
                    "sleep.sleep.isMainSleep": true},
                   {fields: {
                     "date": true,
                     "tags": true,
                     "sleep.summary": true,
                     // $elemMatch can't be used on subelements
                     //"sleep.sleep": {$elemMatch: {isMainSleep: true}},
                     "sleep.sleep": true,
                     "activities.summary.steps": true},
                    "sort": {"date": -1}
                   });
});

Meteor.publish("day", function(date) {
  return Days.find({user: this.userId,
                    date: date});
});

'use strict';
var FitbitAPI = new Fitbit();
_ = lodash;

function createSubscription() {
  var userId = Meteor.userId(),
      hasSubscription = false,
      subscriptions = FitbitAPI.get('user/-/apiSubscriptions.json');
  _.each(subscriptions.data.apiSubscriptions,
         function (sub) {
           if (sub.subscriptionId === userId) {
             hasSubscription = true;
           } else {
             console.log('deleting subscription', sub.subscriptionId);
             // no delete method
             FitbitAPI.call(
               'DELETE',
               'user/-/apiSubscriptions/' + sub.subscriptionId + '.json');
           }
         });
  if (!hasSubscription) {
    console.log('creating subscription', userId);
    return FitbitAPI.post(
      'user/-/apiSubscriptions/' + userId + '.json');
  }
  return null;
}

function catchupSync() {
  var user = Meteor.user(),
      userId = user._id;
  console.log('catchup sync', userId, user.lastSync);
  var current = moment(),
      end;
  if (!user.lastSync) {
    createSubscription();
    end = moment(user.profile.memberSince);
  } else {
    end = moment(user.lastSync);
  }
  while (current >= end) {
    Syncs.upsert({date: current.format('YYYY-MM-DD'),
                  user: userId,
                  type: 'sleep'},
                 {$set: {type: 'sleep'}});
    Syncs.upsert({date: current.subtract("days", 1).format('YYYY-MM-DD'),
                  user: userId,
                  type: 'activities'},
                 {$set: {type: 'activities'}});
    // the activity upsert does this, since subtract technically has
    // side-effects
    // current = current.subtract("days", 1);
  }
  Meteor.users.update({_id: userId},
                      {$set: {lastSync: new Date()}});
}


Meteor.methods({
  setTags: function(date, tags) {
    return Days.update({user: Meteor.userId(),
                        date: date},
                       {"$set": {
                  tags: tags}});
  },

  getTags: function() {
    var tagsMap = {};
    Days.find({tags: {$exists: true}}, {fields: {tags: 1}}).forEach(
      function(day) {
        _.each(day.tags, function(tag) {
          // strip spaces
          var match = /^\s*(\S+)\s*$/.exec(tag);
          if (match) {
            tag = match[1];
          } else {
            return;
        }
          if (tagsMap[tag]) {
            tagsMap[tag] += 1;
          } else {
            tagsMap[tag] = 1;
          }
        });
      });
    return tagsMap;
  },

  subscriptions: function() {
    return FitbitAPI.get('user/-/apiSubscriptions.json');
  },

  syncSleep: function() {
    var user = Meteor.user();
    if (!user) {
      return;
    }
    var userId = user._id;
    this.unblock();
    catchupSync();
    console.log('syncingSleep',  userId);
    UserSession.set('syncingSleep', true, userId);
    var count = 0,
        err;
    try {
      Syncs.find({user: userId}, {sort: {date: -1}}).forEach(function(sync) {
        console.log('syncing', sync.user, sync.date, sync.type);
        switch (sync.type) {
        case "sleep":
          Meteor.call("getSleep", sync.date);
          break;
        case "activities":
          Meteor.call("getActivities", sync.date);
          break;
        }
        Syncs.remove({_id: sync._id});
        count += 1;
      });
    } catch (e) {
      console.log(e);
      err = e;
      // handled later
    }
    UserSession.set('syncingSleep', false, userId);
    UserSession.set('needsSync', false, userId);
    if (!err) {
      console.log('finished', userId, 'synced ', count);
    } else {
      throw err;
    }
    return count;
  },

  getSleep: function(date) {
    console.log('getSleep', Meteor.userId(), date);
    this.unblock();
    var result = FitbitAPI.get(
      'user/-/sleep/date/' + date + '.json');
    console.log(result);
    Days.upsert({
      user: Meteor.userId(),
      date: date},
                {"$set": {
                  sleep:{
                    sleep: result.data.sleep,
                    summary: result.data.summary}}
                });
    return result;
  },

  getActivities: function(date) {
    console.log('getActivities', Meteor.userId(), date);
    this.unblock();
    var result = FitbitAPI.get(
      'user/-/activities/date/' + date + '.json');
    // today's activities affect tomorrow's sleep
    console.log(result);
    Days.upsert({
      user: Meteor.userId(),
      date: moment(date).add("days", 1).format('YYYY-MM-DD')},
                {"$set": {
                  activities: {
                    activities: result.data.activities,
                    summary: result.data.summary}}
                });
    return result;
  }
});
